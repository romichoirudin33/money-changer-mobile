package com.example.moneychangermobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moneychangermobile.Retrofit.ApiClient;
import com.example.moneychangermobile.Retrofit.AuthInterface;
import com.example.moneychangermobile.Retrofit.Models.Token;
import com.example.moneychangermobile.Retrofit.Models.User;
import com.example.moneychangermobile.Utils.Util;
import com.google.android.material.snackbar.Snackbar;

import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText ed1, ed2;
    Button btn;
    Util util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        util = new Util(this);

        ed1 = (EditText)findViewById(R.id.txt_email);
        ed2 = (EditText)findViewById(R.id.txt_password);
        btn = (Button) findViewById(R.id.btn);

        ed1.setText("superadministrator@app.com");
        ed2.setText("password");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed1.getText().toString().equals("") || ed2.getText().toString().equals("")) {
                    Snackbar.make(view, "Inputan tidak boleh kosong!", Snackbar.LENGTH_SHORT).show();
                } else {
                    login(ed1.getText().toString(), ed2.getText().toString());
                }
            }
        });
    }

    AuthInterface authInterface;
    public void login(String username, String password){
        util.showDialog("Tunggu Sebentar...");
        authInterface = ApiClient.getClient().create(AuthInterface.class);
        Call<Token> call = authInterface.getToken(username, password);
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.message().equals("OK")) {
                    util.hideDialog();
                    String resToken = response.body().getAccessToken();
                    Log.d("info", resToken);
                    util.simpanToken(resToken);
                    cekUser();
                } else {
                    util.hideDialog();
                    Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.e("RequestFailed", t.getMessage());
                Toast.makeText(LoginActivity.this, fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cekUser(){
        util.showDialog("Pengecekan user login...");
        authInterface = ApiClient.getClient().create(AuthInterface.class);
        Call<User> call = authInterface.getUser(util.getToken());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.message().equals("OK")) {
                    User user = response.body();
                    Log.i("Status", user.getName());
                    Log.i("Status", user.getEmail());
                    util.simpanUser(user.getId(), user.getName(), user.getEmail(), user.getPicture());
                    util.hideDialog();
                    util.pindahActivity(BranchActivity.class);
                    finish();
                } else {
                    util.hideDialog();
                    Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("RequestFailed", t.getMessage());
                Toast.makeText(LoginActivity.this, fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private String fetchErrorMessage(Throwable throwable) {
        util.hideDialog();
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }
}
