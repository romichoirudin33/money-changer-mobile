package com.example.moneychangermobile.Retrofit;

import com.example.moneychangermobile.Retrofit.Models.Token;
import com.example.moneychangermobile.Retrofit.Models.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AuthInterface {

    @FormUrlEncoded
    @POST("login")
    Call<Token> getToken(@Field("username") String user,
                         @Field("password") String pass);

    @GET("user")
    Call<User> getUser(@Query("api_token") String token);
}
