package com.example.moneychangermobile.Retrofit;

import com.example.moneychangermobile.Retrofit.Models.Branch;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BranchInterface {

    String SUB = "v1";

    @GET(SUB+"/branch")
    Call<List<Branch>> getAll();

    @GET(SUB+"/branch/{id}")
    Call<Branch> getId(@Path("id") int branch_id);

}
