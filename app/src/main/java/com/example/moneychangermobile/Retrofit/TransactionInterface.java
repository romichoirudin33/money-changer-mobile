package com.example.moneychangermobile.Retrofit;

import com.example.moneychangermobile.Retrofit.Models.ResponseTransaction;
import com.example.moneychangermobile.Retrofit.Models.Transaction;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TransactionInterface {

    @POST("new-transaction")
    Call<ResponseTransaction> newTransaction(@Body Transaction transaction);

    @POST("add-transaction")
    Call<ResponseTransaction> addTransaction(@Body Transaction transaction);

    @POST("print-transaction")
    Call<ResponseTransaction> printTransaction(@Body Transaction transaction);
}
