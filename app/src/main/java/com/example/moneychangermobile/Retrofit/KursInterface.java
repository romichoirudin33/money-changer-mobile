package com.example.moneychangermobile.Retrofit;

import com.example.moneychangermobile.Retrofit.Models.Kurs;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface KursInterface {

    String SUB = "v1";

    @GET(SUB+"/kurs")
    Call<List<Kurs>> getAll(@Query("cabang_id") int branch_id);

    @GET(SUB+"/kurs/{id}")
    Call<Kurs> getId(@Path("id") int kurs_id);

    @POST(SUB+"/kurs/{id}")
    Call<Kurs> update(@Path("id") int kurs_id, @Body Kurs kurs);

    @POST("get-kurs")
    Call<Kurs> getCurrency(@Body Kurs kurs);
}
