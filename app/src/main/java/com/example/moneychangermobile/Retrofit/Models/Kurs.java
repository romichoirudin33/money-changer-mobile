package com.example.moneychangermobile.Retrofit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kurs {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("curr")
    @Expose
    private String curr;
    @SerializedName("buy")
    @Expose
    private Double buy;
    @SerializedName("sell")
    @Expose
    private Double sell;
    @SerializedName("kas")
    @Expose
    private Double kas;
    @SerializedName("hpp")
    @Expose
    private Double hpp;
    @SerializedName("cabang_id")
    @Expose
    private Integer cabang_id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Kurs() {
    }

    public Kurs(String curr, Double buy, Double sell, Double kas) {
        this.curr = curr;
        this.buy = buy;
        this.sell = sell;
        this.kas = kas;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public Double getBuy() {
        return buy;
    }

    public void setBuy(Double buy) {
        this.buy = buy;
    }

    public Double getSell() {
        return sell;
    }

    public void setSell(Double sell) {
        this.sell = sell;
    }

    public Double getKas() {
        return kas;
    }

    public void setKas(Double kas) {
        this.kas = kas;
    }

    public Double getHpp() {
        return hpp;
    }

    public void setHpp(Double hpp) {
        this.hpp = hpp;
    }

    public Integer getCabang_id() {
        return cabang_id;
    }

    public void setCabang_id(Integer cabang_id) {
        this.cabang_id = cabang_id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
