package com.example.moneychangermobile.Retrofit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionDetail {

    public TransactionDetail(String curr, Double amount, Double kurs, String tipe) {
        this.curr = curr;
        this.amount = amount;
        this.kurs = kurs;
        this.tipe = tipe;
    }

    @SerializedName("transaction_id")
    @Expose
    private Integer transaction_id;

    @SerializedName("curr")
    @Expose
    private String curr;

    @SerializedName("amount")
    @Expose
    private Double amount;

    @SerializedName("kurs")
    @Expose
    private Double kurs;

    @SerializedName("tipe")
    @Expose
    private String tipe;

    public Integer getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(Integer transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getKurs() {
        return kurs;
    }

    public void setKurs(Double kurs) {
        this.kurs = kurs;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }
}
