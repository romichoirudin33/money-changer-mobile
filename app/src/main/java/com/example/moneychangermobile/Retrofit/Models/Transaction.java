package com.example.moneychangermobile.Retrofit.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    public Transaction() {
    }

    public Transaction(String curr, Double amount, Double kurs, String tipe) {
        this.curr = curr;
        this.amount = amount;
        this.kurs = kurs;
        this.tipe = tipe;
    }

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("user_id")
    @Expose
    private Integer user_id;

    @SerializedName("cabang_id")
    @Expose
    private Integer cabang_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("no_identity")
    @Expose
    private String no_identity;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("transaction_id")
    @Expose
    private Integer transaction_id;

    @SerializedName("curr")
    @Expose
    private String curr;

    @SerializedName("amount")
    @Expose
    private Double amount;

    @SerializedName("kurs")
    @Expose
    private Double kurs;

    @SerializedName("tipe")
    @Expose
    private String tipe;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getCabang_id() {
        return cabang_id;
    }

    public void setCabang_id(Integer cabang_id) {
        this.cabang_id = cabang_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo_identity() {
        return no_identity;
    }

    public void setNo_identity(String no_identity) {
        this.no_identity = no_identity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(Integer transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getKurs() {
        return kurs;
    }

    public void setKurs(Double kurs) {
        this.kurs = kurs;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }
}
