package com.example.moneychangermobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneychangermobile.Adapter.BranchAdapter;
import com.example.moneychangermobile.Retrofit.ApiClient;
import com.example.moneychangermobile.Retrofit.BranchInterface;
import com.example.moneychangermobile.Retrofit.Models.Branch;
import com.example.moneychangermobile.Utils.OnItemClickListener;
import com.example.moneychangermobile.Utils.Util;

import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchActivity extends AppCompatActivity {

    Util util;
    private BranchAdapter adapter;
    private RecyclerView recyclerView;
    private TextView txt_empty;
    String TAG = "respon";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch);

        util = new Util(this);

        //cek session
        util = new Util(this);
        if (!util.cekLogin()) {
            util.pindahActivity(LoginActivity.class);
            finish();
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        txt_empty = (TextView)findViewById(R.id.empty_view);
        getBranch();
    }

    BranchInterface client;
    public void getBranch(){
        util.showDialog("Mengambil data...");
        client = ApiClient.getClient().create(BranchInterface.class);
        Call<List<Branch>> call = client.getAll();
        call.enqueue(new Callback<List<Branch>>() {
            @Override
            public void onResponse(Call<List<Branch>> call, final Response<List<Branch>> response) {
                util.hideDialog();
                if (response.body().size() > 0) {
                    txt_empty.setVisibility(View.GONE);
                } else {
                    txt_empty.setVisibility(View.VISIBLE);
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                adapter = new BranchAdapter(getApplicationContext(), response.body());
                adapter.setListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position, boolean isLongClick) {
                        String id = response.body().get(position).getId().toString();
                        String name = response.body().get(position).getName();
                        util.saveBranch(id, name);
                        util.pindahActivity(MainActivity.class);
                        finish();
                    }
                });
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Branch>> call, Throwable t) {
                util.hideDialog();
                Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: "+t.toString());
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private String fetchErrorMessage(Throwable throwable) {
        util.hideDialog();
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }
}
