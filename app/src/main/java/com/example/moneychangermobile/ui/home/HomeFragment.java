package com.example.moneychangermobile.ui.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moneychangermobile.Adapter.KursAdapter;
import com.example.moneychangermobile.R;
import com.example.moneychangermobile.Retrofit.ApiClient;
import com.example.moneychangermobile.Retrofit.KursInterface;
import com.example.moneychangermobile.Retrofit.Models.Kurs;
import com.example.moneychangermobile.Utils.OnItemClickListener;
import com.example.moneychangermobile.Utils.Util;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    TextView textView, txt_empty;
    RecyclerView recyclerView;
    KursAdapter adapter;
    Util util;
    String TAG = "respon";

    int id = 0;

    EditText txt1,txt2,txt3;
    Button btn1, btn2;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        util = new Util(getContext());

        textView = root.findViewById(R.id.text_home);
        textView.setText(getResources().getString(R.string.branch) + " " + util.getBranchName());

        txt1 = root.findViewById(R.id.txt_currency);
        txt2 = root.findViewById(R.id.txt_buy);
        txt3 = root.findViewById(R.id.txt_sell);

        btn1 = root.findViewById(R.id.btn);
        btn2 = root.findViewById(R.id.btn_cancel);

        recyclerView = root.findViewById(R.id.recycle_view);
        txt_empty = root.findViewById(R.id.empty_view);

        getKurs(util.getBranchId());

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        return root;
    }

    KursInterface client;
    public void getKurs(String branchId){
        util.showDialog("Mengambil data ...");
        client = ApiClient.getClient().create(KursInterface.class);
        Call<List<Kurs>> call = client.getAll(Integer.valueOf(branchId));
        call.enqueue(new Callback<List<Kurs>>() {
            @Override
            public void onResponse(Call<List<Kurs>> call, final Response<List<Kurs>> response) {
                util.hideDialog();
                if (response.body().size() > 0) {
                    txt_empty.setVisibility(View.GONE);
                } else {
                    txt_empty.setVisibility(View.VISIBLE);
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                adapter = new KursAdapter(getContext(), response.body());

                adapter.setListener(new OnItemClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onItemClick(View view, int position, boolean isLongClick) {
                        id = response.body().get(position).getId();
                        txt1.setText(response.body().get(position).getCurr());
                        txt2.setText(response.body().get(position).getBuy().toString());
                        txt3.setText(response.body().get(position).getSell().toString());
                        txt2.setEnabled(true);
                        txt3.setEnabled(true);
                    }
                });
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
                reset();
            }

            @Override
            public void onFailure(Call<List<Kurs>> call, Throwable t) {
                util.hideDialog();
                Toast.makeText(getContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: "+t.toString());
            }
        });
    }

    private void reset(){
        id = 0;
        txt1.setText("");
        txt2.setText("");
        txt3.setText("");
        txt2.setEnabled(false);
        txt3.setEnabled(false);
    }

    private void edit(){
        if (txt2.getText().toString().equals("") || txt3.getText().toString().equals("")) {
            Snackbar.make(getView(), "Inputan tidak boleh kosong!", Snackbar.LENGTH_SHORT).show();
        } else {
            client = ApiClient.getClient().create(KursInterface.class);
            Kurs kurs = new Kurs();
            kurs.setId(id);
            Log.d(TAG, "onFailure: "+id);
            kurs.setBuy(Double.valueOf(txt2.getText().toString()));
            kurs.setSell(Double.valueOf(txt3.getText().toString()));
            Call<Kurs> call = client.update(id, kurs);
            call.enqueue(new Callback<Kurs>() {
                @Override
                public void onResponse(Call<Kurs> call, Response<Kurs> response) {
                    getKurs(util.getBranchId());
                    Log.d(TAG, "onFailure: "+response.body());
                }

                @Override
                public void onFailure(Call<Kurs> call, Throwable t) {
                    Toast.makeText(getContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onFailure: "+t.toString());
                }
            });
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private String fetchErrorMessage(Throwable throwable) {
        util.hideDialog();
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }
}