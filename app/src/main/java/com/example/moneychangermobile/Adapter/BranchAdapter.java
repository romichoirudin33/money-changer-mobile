package com.example.moneychangermobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moneychangermobile.R;
import com.example.moneychangermobile.Retrofit.Models.Branch;
import com.example.moneychangermobile.Utils.OnItemClickListener;

import java.util.List;

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.MyViewHolder> {

    private Context context;
    private List<Branch> list;

    public BranchAdapter(Context context, List<Branch> list) {
        this.context = context;
        this.list = list;
    }

    static OnItemClickListener listener;

    public static OnItemClickListener getListener() {
        return listener;
    }

    public static void setListener(OnItemClickListener listener) {
        BranchAdapter.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt1, txt2, txt3;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt1 = itemView.findViewById(R.id.branch_name);
            txt2 = itemView.findViewById(R.id.branch_address);
            txt3 = itemView.findViewById(R.id.branch_hotline);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, getLayoutPosition(), false);
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.branch_row, parent, false);
        return new BranchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Branch obj = list.get(position);
        holder.txt1.setText(obj.getName());
        holder.txt2.setText(obj.getAddress());
        holder.txt3.setText(obj.getHotline());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
