package com.example.moneychangermobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moneychangermobile.R;
import com.example.moneychangermobile.Retrofit.Models.Kurs;
import com.example.moneychangermobile.Utils.OnItemClickListener;

import java.util.List;

public class KursAdapter extends RecyclerView.Adapter<KursAdapter.MyViewHolder>{

    private Context context;
    private List<Kurs> list;

    public KursAdapter(Context context, List<Kurs> list) {
        this.context = context;
        this.list = list;
    }

    static OnItemClickListener listener;

    public static OnItemClickListener getListener() {
        return listener;
    }

    public static void setListener(OnItemClickListener listener) {
        KursAdapter.listener = listener;
    }

    @NonNull
    @Override
    public KursAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.kurs_row, parent, false);
        return new KursAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull KursAdapter.MyViewHolder holder, int position) {
        Kurs obj = list.get(position);
        holder.txt1.setText(obj.getCurr());
        holder.txt2.setText("Beli : " + String.format("%,.2f", obj.getBuy()));
        holder.txt3.setText("Jual : " + String.format("%,.2f", obj.getSell()));
        holder.txt4.setText("Kas  : " + String.format("%,.2f", obj.getKas()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt1, txt2, txt3, txt4;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt1 = itemView.findViewById(R.id.currency);
            txt2 = itemView.findViewById(R.id.buy);
            txt3 = itemView.findViewById(R.id.sell);
            txt4 = itemView.findViewById(R.id.cash);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, getLayoutPosition(), false);
                    }
                }
            });
        }
    }
}
