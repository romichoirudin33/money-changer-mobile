package com.example.moneychangermobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.moneychangermobile.R;
import com.example.moneychangermobile.Retrofit.Models.Kurs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

public class AutoCompleteCurrencyAdapter extends ArrayAdapter<Kurs> {

    private List<Kurs> kursListFull;


    public AutoCompleteCurrencyAdapter(@NonNull Context context, @NonNull List<Kurs> kursList) {
        super(context, 0, kursList);
        kursListFull = new ArrayList<>(kursList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return kursFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.kurs_row, parent, false
            );
        }

        TextView text_view_curr = convertView.findViewById(R.id.currency);
        TextView text_view_buy = convertView.findViewById(R.id.buy);
        TextView text_view_sell = convertView.findViewById(R.id.sell);
        TextView text_view_cash = convertView.findViewById(R.id.cash);

        Kurs kurs = getItem(position);

        if (kurs != null) {
            text_view_curr.setText(kurs.getCurr());
            text_view_buy.setText("Beli : " +String.format("%,.2f", kurs.getBuy()));
            text_view_sell.setText("Jual : " +String.format("%,.2f", kurs.getSell()));
            text_view_cash.setText("Kas : " +String.format("%,.2f", kurs.getKas()));
        }

        return convertView;
    }

    private Filter kursFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Kurs> suggestions = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(kursListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Kurs item : kursListFull) {
                    if (item.getCurr().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Kurs) resultValue).getCurr();
        }
    };
}
