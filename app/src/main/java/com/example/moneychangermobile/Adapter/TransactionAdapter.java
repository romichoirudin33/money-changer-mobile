package com.example.moneychangermobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.moneychangermobile.R;
import com.example.moneychangermobile.Retrofit.Models.Transaction;
import com.example.moneychangermobile.Utils.OnItemClickListener;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    private Context context;
    private List<Transaction> list;

    public TransactionAdapter(Context context, List<Transaction> list) {
        this.context = context;
        this.list = list;
    }

    static OnItemClickListener listener;

    public static OnItemClickListener getListener() {
        return listener;
    }

    public static void setListener(OnItemClickListener listener) {
        TransactionAdapter.listener = listener;
    }

    @NonNull
    @Override
    public TransactionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transactions_row, parent, false);
        return new TransactionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.MyViewHolder holder, int position) {
        Transaction obj = list.get(position);
        holder.txt1.setText(obj.getCurr());
        holder.txt2.setText(obj.getTipe());
        holder.txt3.setText(String.format("%,.0f", obj.getKurs()) + " x " + String.format("%,.0f", obj.getAmount()));
        holder.txt4.setText("Rp  : " + String.format("%,.0f", obj.getKurs() * obj.getAmount()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt1, txt2, txt3, txt4;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt1 = itemView.findViewById(R.id.currency);
            txt2 = itemView.findViewById(R.id.tipe);
            txt3 = itemView.findViewById(R.id.valuta);
            txt4 = itemView.findViewById(R.id.total);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(v, getLayoutPosition(), false);
                    }
                }
            });

        }
    }
}
