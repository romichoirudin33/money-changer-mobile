package com.example.moneychangermobile.Utils;

import android.view.View;

public interface OnItemClickListener {

    void onItemClick(View view, int position, boolean isLongClick);

}
