package com.example.moneychangermobile.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;

public class Util {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "token_login";
    private static final String TOKEN = "token", IS_LOGIN="status";
    private static final String USER_ID = "user_id";
    private static final String NAME = "name";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String ROLE = "role";
    private static final String PICTURE = "avatar.png";

    private static final String BRANCH_ID = "branch_id";
    private static final String BRANCH_NAME = "branch_name";

    ProgressDialog dialog;

    public Util(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void simpanToken(String token) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public void simpanUser(Integer user_id, String name, String email, String picture){
        editor.putString(USER_ID, String.valueOf(user_id));
        editor.putString(NAME, name);
        editor.putString(EMAIL, email);
        editor.putString(PICTURE, picture);
        editor.commit();
    }

    public void saveBranch(String branch_id, String branch_name){
        editor.putString(BRANCH_ID, branch_id);
        editor.putString(BRANCH_NAME, branch_name);
        editor.commit();
    }

    public String getUserId() {
        return pref.getString(USER_ID, null);
    }

    public String getToken() {
        return pref.getString(TOKEN, null);
    }

    public String getName() {
        return pref.getString(NAME, null);
    }

    public String getUsername() {
        return pref.getString(USERNAME, null);
    }

    public String getEmail() {
        return pref.getString(EMAIL, null);
    }

    public String getPicture() {
        return pref.getString(PICTURE, null);
    }

    public String getBranchId() {
        return pref.getString(BRANCH_ID, null);
    }

    public String getBranchName() {
        return pref.getString(BRANCH_NAME, null);
    }

    public boolean cekLogin() { return pref.getBoolean(IS_LOGIN, false);}

    public void LogoutSession(){
        editor.clear();
        editor.commit();
    }

    public void showDialog(String pesan){
        dialog = ProgressDialog.show(_context,"Proses",pesan);
    }

    public void hideDialog(){
        dialog.dismiss();
    }

    public void pindahActivity(Class<?> tujuan) {
        _context.startActivity(new Intent(_context, tujuan));
    }

    public void setCurrency(final EditText edt) {
        edt.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(current)) {
                    edt.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[Rp.,]", "");
                    double parsed;
                    try {
                        parsed = Double.parseDouble(cleanString);
                    } catch (NumberFormatException e) {
                        parsed = 0.00;
                    }
                    String formatted = NumberFormat.getNumberInstance().format((parsed));

                    current = formatted;
                    edt.setText(formatted);
                    edt.setSelection(formatted.length());

                    edt.addTextChangedListener(this);
                }
            }
        });
    }

    public void getSumRupiah(final EditText valuta, final EditText exchange_rate, final EditText rupiah) {
        valuta.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                double sum, rate, val;
                try{
                    String cleanString;
                    cleanString = exchange_rate.getText().toString().replaceAll("[Rp.,]", "");
                    rate = Double.valueOf(cleanString);

                    cleanString = s.toString().replaceAll("[Rp.,]", "");
                    val = Double.valueOf(cleanString);
                    sum = rate * val;
                }catch (NumberFormatException e){
                    sum = 0;
                }

                rupiah.setText(NumberFormat.getNumberInstance().format((sum)));
            }
        });
    }
}
