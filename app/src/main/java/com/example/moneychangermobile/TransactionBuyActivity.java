package com.example.moneychangermobile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneychangermobile.Adapter.AutoCompleteCurrencyAdapter;
import com.example.moneychangermobile.Adapter.TransactionAdapter;
import com.example.moneychangermobile.Retrofit.ApiClient;
import com.example.moneychangermobile.Retrofit.KursInterface;
import com.example.moneychangermobile.Retrofit.Models.Kurs;
import com.example.moneychangermobile.Retrofit.Models.ResponseTransaction;
import com.example.moneychangermobile.Retrofit.Models.Transaction;
import com.example.moneychangermobile.Retrofit.TransactionInterface;
import com.example.moneychangermobile.Utils.OnItemClickListener;
import com.example.moneychangermobile.Utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionBuyActivity extends AppCompatActivity {

    Util util;
    String TAG = "respon";

    EditText txt_customer_name, txt_exchange_rate, txt_incoming_currency, txt_rupiah_paid;
    EditText ed_customer_name, ed_customer_id, ed_customer_address;
    TextView txt_currency, txt_cash, txt_hpp, txt_total_paid, txt_information, txt_voucher;

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    ImageButton img_btn_customer, img_btn_currency;

    Button btn_change_exchage_rate, btn_add, btn_print;

    AutoCompleteCurrencyAdapter autoCompleteCurrencyAdapter;
    AutoCompleteTextView autoCurrency;

    String customer_id = "", customer_address = "";
    Boolean bedaKurs = false;

    Integer transaction_id = null;

    private RecyclerView recyclerView;
    private TextView txt_empty;
    private TransactionAdapter adapter;

    List<Transaction> transactionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_buy);

        util = new Util(this);

        txt_customer_name = (EditText) findViewById(R.id.txt_customer_name);
        txt_currency = (TextView) findViewById(R.id.currency);
        txt_cash = (TextView) findViewById(R.id.txt_cash);
        txt_hpp = (TextView) findViewById(R.id.txt_hpp);
        txt_total_paid = (TextView) findViewById(R.id.txt_total_paid);
        txt_information = (TextView) findViewById(R.id.txt_information);
        txt_voucher = (TextView) findViewById(R.id.txt_voucher);

        txt_exchange_rate = (EditText) findViewById(R.id.txt_exchange_rate_buy);
        util.setCurrency(txt_exchange_rate);

        txt_incoming_currency = (EditText) findViewById(R.id.txt_incoming_currency);
        util.setCurrency(txt_incoming_currency);

        txt_rupiah_paid = (EditText) findViewById(R.id.txt_rupiah_paid);
        util.getSumRupiah(txt_incoming_currency, txt_exchange_rate, txt_rupiah_paid);

        img_btn_customer = (ImageButton) findViewById(R.id.btn_customer);
        img_btn_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCustomer();
            }
        });

        img_btn_currency = (ImageButton) findViewById(R.id.btn_currency);
        img_btn_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCurrency(util.getBranchId());
            }
        });

        btn_change_exchage_rate = (Button) findViewById(R.id.btn_change_exchange_rate);
        btn_change_exchage_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeExchangeRate();
            }
        });

        btn_add = (Button) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTransaction();
            }
        });

        btn_print = (Button) findViewById(R.id.btn_print);
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                print();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        txt_empty = (TextView) findViewById(R.id.empty_view);

        transactionList = new ArrayList<>();

        newTransaction();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirm")
                .setMessage("Batalkan semua transaksi ini ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    KursInterface client;
    TransactionInterface transactionInterface;

    public void newTransaction(){
        util.showDialog("Mengambil data ...");
        transactionInterface = ApiClient.getClient().create(TransactionInterface.class);

        Transaction transaction = new Transaction();
        transaction.setStatus("buy");
        transaction.setCabang_id(Integer.valueOf(util.getBranchId()));
        transaction.setUser_id(Integer.valueOf(util.getUserId()));

        Call<ResponseTransaction> call = transactionInterface.newTransaction(transaction);
        call.enqueue(new Callback<ResponseTransaction>() {
            @Override
            public void onResponse(Call<ResponseTransaction> call, Response<ResponseTransaction> response) {
                util.hideDialog();

                if (response.code() == 200 && response.body().getStatus()) {
                    transaction_id = response.body().getId();
                    txt_voucher.setText("Voucher : "+ response.body().getId());
                } else {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, ulangi kembali !", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseTransaction> call, Throwable t) {
                util.hideDialog();
                Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });
    }

    public void getKurs(String curr) {
        util.showDialog("Mengambil data ...");
        client = ApiClient.getClient().create(KursInterface.class);

        Kurs body = new Kurs();
        body.setCurr(curr);
        body.setCabang_id(Integer.valueOf(util.getBranchId()));
        Call<Kurs> call = client.getCurrency(body);
        call.enqueue(new Callback<Kurs>() {
            @Override
            public void onResponse(Call<Kurs> call, final Response<Kurs> response) {
                util.hideDialog();

                if (response.code() == 200) {
                    txt_currency.setText(response.body().getCurr());
                    txt_exchange_rate.setText(String.format("%,.0f", response.body().getBuy()));
                    txt_cash.setText("KAS : " + String.format("%,.0f", response.body().getKas()));
                    txt_hpp.setText("HPP : " + String.format("%,.0f", response.body().getHpp()));
                } else {
                    txt_currency.setText("Belum dipilih");
                    txt_exchange_rate.setText(null);
                    txt_cash.setText(null);
                    txt_hpp.setText(null);
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, ulangi kembali !", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Kurs> call, Throwable t) {
                util.hideDialog();
                Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });
    }

    public void dialogCustomer() {
        dialog = new AlertDialog.Builder(TransactionBuyActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.form_customer, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Biodata Customer");

        ed_customer_name = (EditText) dialogView.findViewById(R.id.ed_customer_name);
        ed_customer_id = (EditText) dialogView.findViewById(R.id.ed_customer_id);
        ed_customer_address = (EditText) dialogView.findViewById(R.id.ed_customer_address);

        dialog.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                txt_customer_name.setText(ed_customer_name.getText().toString());
                customer_id = ed_customer_id.getText().toString();
                customer_address = ed_customer_address.getText().toString();

                dialog.dismiss();
            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void dialogCurrency(String branchId) {
        dialog = new AlertDialog.Builder(TransactionBuyActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.form_currency, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Pilih Vallas");

        autoCurrency = (AutoCompleteTextView) dialogView.findViewById(R.id.auto_currency);

        util.showDialog("Mengambil data ...");
        client = ApiClient.getClient().create(KursInterface.class);
        Call<List<Kurs>> call = client.getAll(Integer.valueOf(branchId));
        call.enqueue(new Callback<List<Kurs>>() {
            @Override
            public void onResponse(Call<List<Kurs>> call, final Response<List<Kurs>> response) {
                util.hideDialog();
                autoCompleteCurrencyAdapter = new AutoCompleteCurrencyAdapter(getApplicationContext(), response.body());
                autoCurrency.setThreshold(1);
                autoCurrency.setAdapter(autoCompleteCurrencyAdapter);
            }

            @Override
            public void onFailure(Call<List<Kurs>> call, Throwable t) {
                util.hideDialog();
                Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });

        dialog.setPositiveButton("PILIH", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getKurs(autoCurrency.getText().toString());
                dialog.dismiss();
            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void changeExchangeRate() {
        bedaKurs = !bedaKurs;

        if (bedaKurs) {
            txt_exchange_rate.setEnabled(true);
            txt_exchange_rate.setBackgroundResource(R.drawable.rounded_edittext);
            btn_change_exchage_rate.setBackgroundResource(R.drawable.btn_primary);
        } else {
            txt_exchange_rate.setEnabled(false);
            txt_exchange_rate.setBackgroundResource(R.drawable.rounded_edittext_disable);
            btn_change_exchage_rate.setBackgroundResource(R.drawable.btn_default);
        }
    }

    public void clear() {
        txt_currency.setText("Belum dipilih");
        txt_exchange_rate.setText(null);
        txt_cash.setText(null);
        txt_hpp.setText(null);
        txt_incoming_currency.setCursorVisible(false);
        txt_incoming_currency.setText(null);
        txt_rupiah_paid.setText(null);
        txt_information.setText("Keterangan");

        txt_exchange_rate.setEnabled(false);
        txt_exchange_rate.setBackgroundResource(R.drawable.rounded_edittext_disable);
        btn_change_exchage_rate.setBackgroundResource(R.drawable.btn_default);
        bedaKurs = false;
    }

    public void addTransaction() {

        String curr = txt_currency.getText().toString();
        String cleanString;
        Double rate, amount;

        try {
            cleanString = txt_exchange_rate.getText().toString().replaceAll("[Rp.,]", "");
            rate = Double.valueOf(cleanString);
        } catch (NumberFormatException e) {
            rate = 0.0;
        }

        try {
            cleanString = txt_incoming_currency.getText().toString().replaceAll("[Rp.,]", "");
            amount = Double.valueOf(cleanString);
        } catch (NumberFormatException e) {
            amount = 0.0;
        }

        if (curr.equalsIgnoreCase("Belum dipilih")) {
            Toast.makeText(this, "Mata uang belum dipilih", Toast.LENGTH_SHORT).show();
        } else if (rate == 0) {
            Toast.makeText(this, "Rate tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else if (amount == 0) {
            Toast.makeText(this, "Valuta tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else {
            String tipe = bedaKurs ? "BEDA KURS" : "STANDART";

            util.showDialog("Mengirim data ...");
            transactionInterface = ApiClient.getClient().create(TransactionInterface.class);

            final Transaction transaction = new Transaction(curr, amount, rate, tipe);
            transaction.setTransaction_id(transaction_id);

            Call<ResponseTransaction> call = transactionInterface.addTransaction(transaction);
            call.enqueue(new Callback<ResponseTransaction>() {
                @Override
                public void onResponse(Call<ResponseTransaction> call, Response<ResponseTransaction> response) {
                    util.hideDialog();

                    if (response.code() == 200 && response.body().getStatus()) {
                        transactionList.add(transaction);
                        updateRecycle();
                        clear();
                    } else {
                        txt_information.setText(response.body().getMessage());
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan, ulangi kembali !", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseTransaction> call, Throwable t) {
                    util.hideDialog();
                    Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onFailure: " + t.toString());
                }
            });

        }
    }

    public void updateRecycle() {
        if (transactionList.size() > 0) {
            txt_empty.setVisibility(View.GONE);
        } else {
            txt_empty.setVisibility(View.VISIBLE);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TransactionAdapter(this, transactionList);
        adapter.setListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, boolean isLongClick) {
                cancelTransaction(position);
            }
        });
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        double total = 0;
        for (Transaction transaction : transactionList) {
            total += transaction.getAmount() * transaction.getKurs();
        }
        txt_total_paid.setText(String.format("%,.0f", total));
    }

    public void cancelTransaction(final int position) {
        dialog = new AlertDialog.Builder(TransactionBuyActivity.this);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Confirm");
        dialog.setMessage("Batalkan transaksi ini ?");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                util.showDialog("Mengirim data ...");
                transactionInterface = ApiClient.getClient().create(TransactionInterface.class);

                final Transaction transaction = transactionList.get(position);
                transaction.setAmount(transactionList.get(position).getAmount() * -1);

                Call<ResponseTransaction> call = transactionInterface.addTransaction(transaction);
                call.enqueue(new Callback<ResponseTransaction>() {
                    @Override
                    public void onResponse(Call<ResponseTransaction> call, Response<ResponseTransaction> response) {
                        util.hideDialog();

                        if (response.code() == 200 && response.body().getStatus()) {
                            transactionList.remove(transactionList.get(position));
                            updateRecycle();
                        } else {
                            txt_information.setText(response.body().getMessage());
                            Toast.makeText(getApplicationContext(), "Terjadi kesalahan, ulangi kembali !", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseTransaction> call, Throwable t) {
                        util.hideDialog();
                        Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onFailure: " + t.toString());
                    }
                });

                dialog.dismiss();
            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void print(){

        if (transactionList.size() == 0 ) {
            Toast.makeText(this, "Transaksi tidak ada !! Masukkan transaksi dahulu", Toast.LENGTH_SHORT).show();
        }else if (txt_customer_name.getText().toString().isEmpty()){
            Toast.makeText(this, "Nama customer tidak boleh kosong !!", Toast.LENGTH_SHORT).show();
        }else{
            util.showDialog("Mengirim data ...");
            transactionInterface = ApiClient.getClient().create(TransactionInterface.class);

            final Transaction transaction = new Transaction();
            transaction.setName(txt_customer_name.getText().toString());
            transaction.setNo_identity(customer_id);
            transaction.setAddress(customer_address);
            transaction.setTransaction_id(transaction_id);

            Call<ResponseTransaction> call = transactionInterface.printTransaction(transaction);
            call.enqueue(new Callback<ResponseTransaction>() {
                @Override
                public void onResponse(Call<ResponseTransaction> call, Response<ResponseTransaction> response) {
                    util.hideDialog();

                    if (response.code() == 200 && response.body().getStatus()) {
                        Toast.makeText(getApplicationContext(), "Transaksi di proses !!", Toast.LENGTH_SHORT).show();
                    } else {
                        txt_information.setText(response.body().getMessage());
                        Toast.makeText(getApplicationContext(), "Terjadi kesalahan, ulangi kembali !", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseTransaction> call, Throwable t) {
                    util.hideDialog();
                    Toast.makeText(getApplicationContext(), fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onFailure: " + t.toString());
                }
            });
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private String fetchErrorMessage(Throwable throwable) {
        util.hideDialog();
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }
}

